﻿**README: GroceryList Project**
V 1.0

Commits were added after each feature upgrade or addition. Test Branch was created when testing new features that were not originally in the program functionality.

V 2.0

All updates to the code to make this project compliant with the resubmission standards has been fulilled.

---

## Contact Information

You can contact the programmer directly using the following information: 

Name: Jamie Cahn

Company: MeowNetStudios

Email: jcahn@meownetstudios.com

Phone: (421) 405-3177


---

## Credits

HandleClientWindowSize() method created by *Keith Webster* Used with permission. Not to be distributed.

--